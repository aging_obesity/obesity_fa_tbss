## This is for documentation of how data was preprocessed in the publication zhang et al. 2018 (�White Matter Microstructural Variability Mediates the Relation between Obesity and Cognition in Healthy Adults.� NeuroImage 172 (February):239�49.)

### The script for preprocessing was reorganized but the content reminds unchanged. Orignially the preprocessing was done in seperated scripts. 

### Preprocessing steps:
#### Note: LIFE Study was done using the twice-refocused spin-echo DTI

2. motion correction (eddy)
	- used eddy_correct (with default -dof 12)
	- rotating bvecs (used script following ENIGMA protocol)
3. skull stripping
	- bet
	- quality check for brain mask
	- correated brain mask manually
5. tensor fitting
	- dtifit
	- quality check for resulting images using QC_ENIGMA protocol
	- quality check for ghost artifact manualy
	

### Tract-Based Spacial Statistic (TBSS) analysis steps:	
1. `tbss_1_preproc *.nii.gz`

2. check slicedir for quanlity

3. `tbss_2_reg -T` (we used a modified version to run this job parallel)

4. `tbss_3_postreg -S`

5. `tbss_4_prestats 0.3`

6. created design matrix files manually due to big sample size

7. Check the order of filenames, in FA directory: `imglob *_FA.*`, matched the ID of participant list

8. `randomise -i all_FA_skeletonised.nii.gz -o [output filename] -d design.mat -t design.con -m mean_FA_skeleton_mask -n 10000 -D --T2 -x --uncorrp`
